# Brulion

Theme for Hugo.


## Installation

From the root of your Hugo site, type the following:

```bash
$ git submodule add https://gitlab.com/tskr/brulion.git themes/brulion
```

## Update

```sh
$ git submodule update --remote themes/brulion
```

## Configuration

```sh
$ echo 'theme = "brulion"' >>config.toml
```

